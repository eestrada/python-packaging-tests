#!python

import sys

from . import do_a_thing

def main():
    print("We're going to do something...")
    do_a_thing()
    print("We successfully did the thing. Exiting with success.")

if __name__ == '__main__':
    sys.exit(main())
