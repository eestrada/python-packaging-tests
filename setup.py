#!/usr/bin/env python3

import pkgtest
from setuptools import setup, find_packages

s = setup(
    name=pkgtest.__name__,
    version=pkgtest.__version__,
    license=pkgtest.__license__,
    description=pkgtest.__description__,
    zip_safe=False,
    packages=find_packages(exclude=['test', 'tests']),
    include_package_data=True,
    entry_points={
        "console_scripts": [
            "pkgtest = pkgtest.main:main",
        ],
    },
    python_requires = ">= 3.4",
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    )
